﻿using booking.Models;
using booking.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace booking.Service.Interface
{
    public interface ITicketService
    {
        Ticket BookTicket(int? id, string email, string performer);
        Task<bool> AddBookingTicket(TicketViewModel ticket);
        IEnumerable<Ticket> GetAllTickets();
        Ticket GetTicketById(int? id);
        public bool CheckAvailableTicketsByConcertId(int? idConcert, uint amountTickets);
        void changeAmountTicketsForSelectConcert(int? idConcert, uint amountBookTicket, bool isIncreaseAmount);
        List<Ticket> GetListTicketsByEmail(string email);
        void CancelBooking(int id);
        string GetUserNameByTicketId(int idTicket);
    }
}
