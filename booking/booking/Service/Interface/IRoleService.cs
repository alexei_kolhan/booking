﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace booking.Service
{
    public interface IRoleService
    {
        Task<IdentityUser> GetAdminRole(string id);
        Task<IdentityUser> GetUserRole(string id);
    }
}