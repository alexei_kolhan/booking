﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace booking.Service.Interface
{
    public interface IGenericService<T> where T : class
    {
        T GetById(int? id);
        IEnumerable<T> GetAll();
        IEnumerable<T> Find(Expression<Func<T, bool>> expression);
        void Add(T entity);       
        void Remove(T entity);
        void Save();
        int Update(T entity);
    }
}
