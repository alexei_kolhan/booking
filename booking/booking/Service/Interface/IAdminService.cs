﻿using booking.ViewModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace booking.Service
{
    public interface IAdminService
    {
        void DeleteUser(string id);
        void DeleteUserRole(string id, string role);        
        void EditUser(UserModel userModel, IServiceProvider serviceProvider);
        IEnumerable<UserModel> GetListUserWithRoles();
        UserModel GetUserModel(string id);
        IEnumerable<Microsoft.AspNetCore.Identity.IdentityUser> GetUsers();
    }
}