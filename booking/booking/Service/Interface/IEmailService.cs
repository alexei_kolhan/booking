﻿using RestSharp;
using System.Threading.Tasks;

namespace booking.Service.Interface
{
    public interface IEmailService
    {
        public Task SendEmailAsync(string email, string subject, string message);
    }
}
