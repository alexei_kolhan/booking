﻿using booking.Data;
using booking.Models;
using booking.Service.Interface;
using booking.ViewModel;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace booking.Service
{
    public class TicketService : ITicketService
    {
        private readonly ApplicationDbContext _context;

        public TicketService(ApplicationDbContext context)
        {
            _context = context;
        }

        public Ticket BookTicket(int? idConcert, string email, string performer)
        {
            var concert = _context.Concerts.FirstOrDefaultAsync(u => u.Id == idConcert);
            var user = _context.Users.FirstOrDefaultAsync(u => u.Email == email);

            Ticket ticket = new Ticket();
            ticket.ConcertId = idConcert;
            ticket.performer = performer;
            ticket.UserName = email;

            return ticket;
        }

        public async Task<bool> AddBookingTicket(TicketViewModel ticketVm)
        {
            if (ticketVm == null)
            {
                return false;
            }
            else 
            {
                _context.Add(ticketVm.ticket);
                await _context.SaveChangesAsync();

                return true;
            }
        }

        public IEnumerable<Ticket> GetAllTickets()
        {
            return _context.Tickets.ToList();
        }

        public List<Ticket> GetListTicketsByEmail(string email)
        {
            return _context.Tickets.Where(e => e.UserName == email).ToList();
        }

        public Ticket GetTicketById(int? id)
        {
            var ticket =_context.Tickets.FirstOrDefault(i => i.Id == id);
            return ticket;
        }

        public bool CheckAvailableTicketsByConcertId(int? idConcert, uint amountTickets)
        {
            var concert = _context.Concerts.Find(idConcert);
            if (concert.AmounTickets < amountTickets)
            {
                return false;
            }

            return true;
        }

        public void changeAmountTicketsForSelectConcert(int? idConcert, uint amountBookTicket, bool isIncreaseAmount)
        {
            var concert = _context.Concerts.SingleOrDefault( c => c.Id == idConcert);

            if (concert != null)
            {
                if (isIncreaseAmount)
                {
                    concert.AmounTickets += amountBookTicket;
                    _context.SaveChanges();
                }
                else
                {
                    concert.AmounTickets -= amountBookTicket;
                    _context.SaveChanges();
                }
            }            
        }

        public void CancelBooking(int idTicket)
        {
            var ticket = _context.Tickets.SingleOrDefault( c => c.Id == idTicket);
            changeAmountTicketsForSelectConcert(ticket.ConcertId, ticket.AmountPlaces, true);

            _context.Tickets.Remove(ticket);
            _context.SaveChanges();
        }

        public string GetUserNameByTicketId(int idTicket)
        {
            var ticket = _context.Tickets.SingleOrDefault(c => c.Id == idTicket);

            return ticket.UserName;
        }
    }
}
