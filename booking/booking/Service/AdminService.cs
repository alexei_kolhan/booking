﻿using booking.Data;
using booking.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace booking.Service
{
    [Authorize]
    public class AdminService : IAdminService
    {
        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _serviceProvider;

        public AdminService(ApplicationDbContext context, IServiceProvider serviceProvider)
        {
            _context = context;
            _serviceProvider = serviceProvider;
        }

        public IEnumerable<UserModel> GetListUserWithRoles()
        {
            var listuserRoles = _context.UserRoles.ToList();
            List<UserModel> userModels = new List<UserModel>();

            foreach (var item in listuserRoles)
            {
                string roleId = item.RoleId;
                string roleName = _context.Roles.SingleOrDefault(m => m.Id == roleId).Name;
                string id = item.UserId;
                string email = _context.Users.SingleOrDefault(m => m.Id == id).Email;
                userModels.Add(new UserModel { Id = id, Email = email, Role = roleName });                
            }

            return userModels;
        }

        public void DeleteUserRole(string id, string role)
        {
            string roleId = _context.Roles.SingleOrDefault(m => m.Name == role).Id;
            var userRole = _context.UserRoles.SingleOrDefault(m => m.UserId == id && m.RoleId == roleId);

            if (userRole != null)
            {
                _context.UserRoles.Remove(userRole);
                _context.SaveChanges();               
            } 
        }
        [Authorize(Roles ="Admin")]
        public void DeleteUser(string id)
        {
            var user = _context.Users.SingleOrDefault(m => m.Id == id);

            if (user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
            }
        }

        public UserModel GetUserModel(string id)
        {
            var user = _context.Users.SingleOrDefault(m => m.Id == id);

            UserModel userModel = new UserModel();
            userModel.Id = user.Id;
            userModel.Email = user.Email;

            return userModel;           
        }

        [Authorize(Roles = "Admin")]
        public void EditUser(UserModel userModel, IServiceProvider serviceProvider)
        {
            var UserManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();

            IdentityUser user = _context.Users.FirstOrDefault(m => m.Id == userModel.Id);
            Task<IdentityResult> changeEmail = UserManager.ChangeEmailAsync(user, userModel.Email, null);
            changeEmail.Wait();

            Task<IdentityResult> saveResult = UserManager.UpdateAsync(user);
            saveResult.Wait();

            _context.SaveChangesAsync();
        }

        public IEnumerable<Microsoft.AspNetCore.Identity.IdentityUser> GetUsers()
        {
            
            var list = _context.Users.ToList();            
            return list;
        }
    }
}
