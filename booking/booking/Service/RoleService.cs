﻿using booking.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace booking.Service
{
    public class RoleService : IRoleService
    {
        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _serviceProvider;

        public RoleService(ApplicationDbContext context, IServiceProvider serviceprovider)
        {
            _context = context;
            _serviceProvider = serviceprovider;
        }

        public async Task<IdentityUser> GetAdminRole(string id)
        {
            var UserManager = _serviceProvider.GetRequiredService<UserManager<IdentityUser>>();

            IdentityUser user = await _context.Users.FirstOrDefaultAsync(m => m.Id == id);

            await UserManager.AddToRoleAsync(user, "Admin");
            await _context.SaveChangesAsync();

            return user;
        }

        public async Task<IdentityUser> GetUserRole(string id)
        {
            var UserManager = _serviceProvider.GetRequiredService<UserManager<IdentityUser>>();

            IdentityUser user = await _context.Users.FirstOrDefaultAsync(m => m.Id == id);

            await UserManager.AddToRoleAsync(user, "User");
            await _context.SaveChangesAsync();

            return user;
        }
    }
}
