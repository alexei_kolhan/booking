﻿using System.ComponentModel.DataAnnotations;

namespace booking.Models
{
    public class Classic : Concert
    {
        [Required]
        public string TypeOfConcert = "Classic";

        [Required]
        public string TypeOfVoice { get; set; }
        
        [Required]
        public string NameOfConcert { get; set; }

        [Required]
        public string NameOfComposer { get; set; }
    }
}
