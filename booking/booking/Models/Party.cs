﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace booking.Models
{
    public class Party : Concert
    {
        public string TypeOfConcert = "Party";
        [Required]
        public uint minimalAge { get; set; }
        [Required]
        public uint maximalAge { get; set; } 
    }
}
