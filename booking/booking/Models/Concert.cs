﻿using System;

namespace booking.Models
{
    public class Concert
    {
        public int Id { get; set; }
        public string Performer { get; set; }
        public uint AmounTickets { get; set; }
        public DateTime Date { get; set; }
        public string Geo_lat { get; set; }
        public string Geo_lng { get; set; }        

    }
}
