﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace booking.Models
{
    public class Ticket
    {
        public int Id { get; set; }        

        [Required]
        [EmailAddress(ErrorMessage = "Некорректный адрес")]
        public string UserName { get; set; }

        [Required]
        public int? ConcertId { get; set; }

        [Required]
        [Range(1, 1000, ErrorMessage = "Недопустимое количество билетов")]
        public uint AmountPlaces { get; set; }

        [Required]
        [NotMapped]
        public string performer { get; set; }        
    }
}
