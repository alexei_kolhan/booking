﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace booking.Models
{
    public class OpenAir: Concert
    {
        public string TypeOfConcert = "OpenAir";
        [Required]
        public string Headliner { get; set; }
        [Required]
        public string PathToOpenAir { get; set; }
    }
}
