﻿using booking.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace booking.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {

        public DbSet<Concert> Concerts { get; set; }

        public DbSet<Party> Parties { get; set; }
        public DbSet<OpenAir> OpenAirs { get; set; }
        public DbSet<Classic> Classics { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {


        }              
    }
}
