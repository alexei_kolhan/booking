﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace booking.Data
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ApplicationDbContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ApplicationDbContext>>()))
            {
                // Look for any movies.
                if (context.Concerts.Any())
                {
                    return;   // DB has been seeded
                }
                else
                {
                    context.Concerts.AddRange(

                        new Models.Party
                        {
                            Performer = "William",
                            Date = DateTime.Now,
                            AmounTickets = 350,
                            minimalAge = 16,
                            maximalAge = 40,
                            Geo_lat = "53.21312",
                            Geo_lng = "34.34343",
                        },

                        new Models.Party
                        {
                            Performer = "jack",
                            Date = DateTime.Now,
                            AmounTickets = 250,
                            minimalAge = 10,
                            maximalAge = 30,
                            Geo_lat = "23.21312",
                            Geo_lng = "24.34343",
                        },

                        new Models.OpenAir
                        {
                            Performer = "Lili",
                            Date = DateTime.Now,
                            AmounTickets = 450,
                            Headliner = "John",
                            PathToOpenAir = "path",
                            Geo_lat = "14.123123",
                            Geo_lng = "16.231231"

                        },
                        new Models.OpenAir
                        {
                            Performer = "Alex",
                            Date = DateTime.Now,
                            AmounTickets = 50,
                            Headliner = "John_2",
                            PathToOpenAir = "path_2",
                            Geo_lat = "64.123123",
                            Geo_lng = "36.231231",
                        },

                        new Models.Classic
                        {
                            Performer = "Kate",
                            Date = DateTime.Now,
                            AmounTickets = 650,
                            NameOfComposer = "Patric",
                            NameOfConcert = "Beautiful concert",
                            TypeOfVoice = "tenor",
                            Geo_lat = "55.21312",
                            Geo_lng = "36.34343",
                        },
                        new Models.Classic
                        {
                            Performer = "Nick",
                            Date = DateTime.Now,
                            AmounTickets = 750,
                            NameOfComposer = "Kuku",
                            NameOfConcert = "Sad concert",
                            TypeOfVoice = "bas",
                            Geo_lat = "25.21312",
                            Geo_lng = "26.34343",
                        }
                    );
                    context.SaveChanges();
                }               
            }
        }

        public static void CreateRoles(IServiceProvider serviceProvider)
        {

            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();
            Task<IdentityResult> roleResult;

            //Check that there is an Administrator role and create if not
            Task<bool> hasAdminRole = roleManager.RoleExistsAsync("Admin");
            hasAdminRole.Wait();

            if (!hasAdminRole.Result)
            {
                roleResult = roleManager.CreateAsync(new IdentityRole("Admin"));
                roleResult.Wait();
            }

            Task<bool> hasUserRole = roleManager.RoleExistsAsync("User");
            hasAdminRole.Wait();

            if (!hasUserRole.Result)
            {
                roleResult = roleManager.CreateAsync(new IdentityRole("User"));
                roleResult.Wait();
            }            
        }
    }
}
