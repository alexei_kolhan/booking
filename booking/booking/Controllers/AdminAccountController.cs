﻿using booking.Service;
using booking.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace booking.Controllers
{
    [Authorize]
    public class AdminAccountController : Controller
    {
        private readonly IAdminService _adminService;
        private readonly IServiceProvider _serviceProvider;

        public AdminAccountController( IAdminService adminService, IServiceProvider serviceProvider)
        {            
            _adminService = adminService;
            _serviceProvider = serviceProvider;
        }

        public IActionResult Index()
        { 
            UserViewModel userViewModel = new UserViewModel();
            userViewModel.userModels = _adminService.GetListUserWithRoles();
            userViewModel.identityUsers = _adminService.GetUsers();
            userViewModel.userNameIsAdmin = User.IsInRole("");
            

            return View(userViewModel);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult DeleteUserRole(string id, string role)
        {
            _adminService.DeleteUserRole(id, role);
            return RedirectToAction(nameof(Index));
        }
       
        [Authorize(Roles = "Admin")]
        public ActionResult EditUser(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }

            var userModel = _adminService.GetUserModel(id);
            if (userModel == null)
            {
                return NotFound();
            }
            return View(userModel);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult EditUser(UserModel userModel)
        {
            _adminService.EditUser(userModel, _serviceProvider);

            return RedirectToAction(nameof(Index));
        }
    }
}
