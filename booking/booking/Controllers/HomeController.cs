﻿using booking.Models;
using booking.Service.Interface;
using booking.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace booking.Controllers
{
    public class HomeController : Controller
    {
        private readonly IGenericService<Concert> _concertService;

        public HomeController(IGenericService<Concert> concertService)
        {            
            _concertService = concertService;
        }

        public IActionResult HomePage(string typeConcert, int month)
        {
            IEnumerable<Concert> listConcerts = _concertService.GetAll();

            IEnumerable<string> concModel = listConcerts.Select(c => new ConcertModel { typeOfConcert = c.GetType().Name })
                                                 .Select(x => x.typeOfConcert).Distinct()
                                                 .ToList();

            ConcertViewModel concViewModel = new() { ConcertModels = concModel, Concerts = listConcerts };

            Dictionary<int, string> listMonth = Enumerable.Range(1, 12).Select(i => new { I = i, M = DateTimeFormatInfo.CurrentInfo.GetMonthName(i) })
                                                                       .ToDictionary(x => x.I, y => y.M);

            concViewModel.Month = listMonth;
            concViewModel.email = User.Identity.Name;

            concViewModel.Concerts = concViewModel.FilterConcertViewModel(listConcerts, typeConcert, month);
            return View(concViewModel);
        }
    }
}
