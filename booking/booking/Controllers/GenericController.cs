﻿using booking.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace booking.Controllers
{
    public class GenericController<T> : Controller where T: class 
    {
        private readonly IGenericService<T> _genericService;

        public GenericController(IGenericService<T> genericService)
        {
            _genericService = genericService;
        }
        // GET: GenericController
        public ActionResult Index()
        {
            return View(_genericService.GetAll());
        }

        // GET: GenericController/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: GenericController/Create
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(T concert)
        {
            if (ModelState.IsValid)
            {
                _genericService.Add(concert);

                return RedirectToAction(nameof(Index));
            }
            return View(concert);
        }

        // GET: GenericController/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id)
        {            
            return View(_genericService.GetById(id));
        }

        // POST: GenericController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id, T concert)
        {
            if (ModelState.IsValid)
            {
                _genericService.Update(concert);

                return RedirectToAction(nameof(Index));
            }
            return View(concert);
        }

        // GET: GenericController/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            return View(_genericService.GetById(id));
        }

        // POST: GenericController/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, T concert)
        {
            _genericService.Remove(_genericService.GetById(id));

            return RedirectToAction(nameof(Index));
        }
    }
}
