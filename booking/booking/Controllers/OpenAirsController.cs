﻿using booking.Models;
using booking.Service.Interface;

namespace booking.Controllers
{
    public class OpenAirsController : GenericController<OpenAir>
    {

        public OpenAirsController(IGenericService<OpenAir> genericService) : base(genericService)
        {
  
        }
        
    }
}
