﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using booking.Models;
using booking.Service.Interface;
using booking.Service;
using booking.ViewModel;

namespace booking.Controllers
{
    public class TicketsController : Controller
    {
        private readonly ITicketService _ticketService;
        private readonly IEmailService _emailService;

        public TicketsController(ITicketService ticketService, IEmailService emailService)
        {
            _ticketService = ticketService;
            _emailService = emailService;
        }

        //// GET: Tickets
        [HttpGet]
        public IActionResult Index()
        {
            return View(_ticketService.GetAllTickets());
        }

        public IActionResult GetAllTicketByEmail(string userName)
        {
            TicketViewModel tViewModel = new TicketViewModel();
            tViewModel.listTickets = _ticketService.GetListTicketsByEmail(userName);

            return View(tViewModel.listTickets);
        }

        // GET: Tickets/Details/5
        [HttpGet]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket =  _ticketService.GetTicketById(id);

            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        [HttpGet]
        public IActionResult BookTicket(int? idConcert, string Email, string performer)
        {
            if (idConcert == null)
            {
                return NotFound();
            }
            if(String.IsNullOrEmpty(Email))
            {
                return Redirect("~/Identity/Account/Login");
            }

            TicketViewModel ticketVM = new TicketViewModel(); 
            ticketVM.ticket = _ticketService.BookTicket(idConcert, Email, performer);

            return View(ticketVM);
        }        

        [HttpPost]
        public async Task<IActionResult> BookTicketAsync(TicketViewModel ticketVm)
        {
            if (ModelState.IsValid)
            {
                if (await _ticketService.AddBookingTicket(ticketVm))
                {
                    if (_ticketService.CheckAvailableTicketsByConcertId(ticketVm.ticket.ConcertId, ticketVm.ticket.AmountPlaces))
                    {
                        _ticketService.changeAmountTicketsForSelectConcert(ticketVm.ticket.ConcertId, ticketVm.ticket.AmountPlaces, false);

                        await _emailService.SendEmailAsync(ticketVm.ticket.UserName,
                                                            "Подтверждение бронирования билета(ов) для " + ticketVm.ticket.UserName,
                                                            "Вы оформили бронь на концерт исполнителя - " + ticketVm.ticket.performer + ", количество бронируемых мест - " + ticketVm.ticket.AmountPlaces);

                    }
                }
                return RedirectToAction("GetAllTicketByEmail", new { userName = ticketVm.ticket.UserName }); 
            }
            else 
            {
                return View(ticketVm);                
            }            
        }

        [HttpGet]
        public IActionResult CancelBooking(int idTicket)
        {
            if (idTicket == 0)
            {
                return NotFound();
            }
            var userName = _ticketService.GetUserNameByTicketId(idTicket);
            _ticketService.CancelBooking(idTicket);

            return RedirectToAction("GetAllTicketByEmail", new { userName = userName });
        }
    }
}
