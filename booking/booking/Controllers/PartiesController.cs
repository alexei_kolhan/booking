﻿using booking.Models;
using booking.Service.Interface;

namespace booking.Controllers
{
    public class PartiesController : GenericController<Party>
    {
        public PartiesController(IGenericService<Party> genericService) : base(genericService)
        {
            
        }       

    }
}
