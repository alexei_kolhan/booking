﻿using booking.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace booking.Controllers
{
    [Authorize]
    public class RolesController : Controller
    {       

        private readonly IRoleService _roleService;

        public RolesController(IRoleService roleService)
        {
            _roleService = roleService;
            
        }

        [Authorize(Roles = "User")]
        private async Task GiveAdminRoles(string name)
        {
            await _roleService.GetAdminRole(name);                       
        }
        
        private async Task GiveUserRoles(string name)
        {
            await _roleService.GetUserRole(name);
        }

        [Authorize(Roles = "User")]
        public async Task<ActionResult> giveAdminRole(string id)
        {
            await GiveAdminRoles(id);

            return Redirect("~/AdminAccount/Index");
        }

        public async Task<ActionResult> giveUserRole(string id)
        {
            await GiveUserRoles(id);

            return Redirect("~/AdminAccount/Index");
        }
    }
}
