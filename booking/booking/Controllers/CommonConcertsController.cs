﻿using booking.Models;
using booking.Service.Interface;
using booking.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace booking.Controllers
{
    public class CommonConcertsController : Controller
    {
        private readonly IGenericService<Concert> _concertService;

        public CommonConcertsController(IGenericService<Concert> concertService)
        {
            _concertService = concertService;
        }

        public IActionResult Index(string typeConcert)
        {
            IEnumerable<Concert> listConcerts = _concertService.GetAll();

            List<string> concModel = listConcerts.Select(c => new ConcertModel { typeOfConcert = c.GetType().Name })
                                                 .Select(x => x.typeOfConcert).Distinct()
                                                 .ToList();

            concModel.Insert(0, "Все" );

            ConcertViewModel concViewModel = new ConcertViewModel { ConcertModels = concModel, Concerts = listConcerts, email= User.Identity.Name};            

            if (!string.IsNullOrEmpty(typeConcert))
            {
                if (typeConcert.Equals("Все"))
                {
                    concViewModel.Concerts = listConcerts;
                }
                else
                {
                    concViewModel.Concerts = listConcerts.Where(p => p.GetType().Name.Equals(typeConcert));
                }
            }

            return View(concViewModel);
        }
    }
}
