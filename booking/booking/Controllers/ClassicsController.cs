﻿using Microsoft.AspNetCore.Mvc;
using booking.Models;
using booking.Service.Interface;
using Microsoft.AspNetCore.Authorization;

namespace booking.Controllers
{
    public class ClassicsController : GenericController<Classic>
    {
        public ClassicsController(IGenericService<Classic> genericService) : base(genericService)
        {
            
        }
        
        
    }
}
