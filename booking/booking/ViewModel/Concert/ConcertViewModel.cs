﻿using booking.Models;
using System.Collections.Generic;
using System.Linq;

namespace booking.ViewModel
{
    public class ConcertViewModel
    {
        public Concert concert { get; set; }
        public IEnumerable<Concert> Concerts { get; set; }
        public IEnumerable<string> ConcertModels { get; set; }
        public Dictionary<int,string> Month { get; set; }
        public string email { get; set; }

        public IEnumerable<Concert> FilterConcertViewModel(IEnumerable<Concert> listConcerts, string typeConcert, int month)
        {
            if (!string.IsNullOrEmpty(typeConcert) && (month >= 1 && month <= 12))
            {
                this.Concerts = listConcerts.Where(p => p.GetType().Name.Equals(typeConcert) && p.Date.Month == month);
            }
            else if (!string.IsNullOrEmpty(typeConcert) && month == 0)
            {
                this.Concerts = listConcerts.Where(p => p.GetType().Name.Equals(typeConcert));
            }
            else if ((month >= 1 && month <= 12) && string.IsNullOrEmpty(typeConcert))
            {
                this.Concerts = listConcerts.Where(p => p.Date.Month == month);
            }

            return this.Concerts;
        }
    }
}
