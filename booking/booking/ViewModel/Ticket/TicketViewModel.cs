﻿
using booking.Models;
using System.Collections.Generic;

namespace booking.ViewModel
{
    public class TicketViewModel
    {
        public Ticket ticket { get; set; }
        public List<Ticket> listTickets { get; set;}
    }
}
