﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace booking.ViewModel
{
    public class UserViewModel
    {
        public IEnumerable<UserModel> userModels { get; set; }

        public IEnumerable<Microsoft.AspNetCore.Identity.IdentityUser> identityUsers { get; set; }

        public bool userNameIsAdmin { get; set; }
    }
}
